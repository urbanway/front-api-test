# Front API test

1. Get API from: https://my-json-server.typicode.com/diamanthaxhimusa/front-api-test/data
2. Display loader as API is being requested
3. Display news seperated in categories
   ex.

      Showbiz:
        card1 card2 card3
      Sport:
        card1 card2 card3

    3.1 Card should contain an image and a title
    3.2 When news-item is clicked show a modal with the news content
    3.3 Truncate title (2 rows)
4. Cache API in session storages
5. Test it
6. Push it to bitbucket


Requirements: Bootstrap 4, javascript, jQuery, Ajax
Duration: 1-3 days

Good luck!
